/**
 * Created by techno23 on 2/1/14.
 */
var myApp= angular.module('myApp',[]);

myApp.factory('Dhoom', function(){
    var Dhoom ={};
    Dhoom.cast =[
        {
            name: "amir",
            character : "vln"
        },
        {
            name: "abi",
            character : "chodu"
        },
        {
            name: "katrina",
            character : "fatka"
        },
        {
            name: "john",
            character : "cool"
        }
    ]
    return Dhoom;
})
function DhoomCtrl($scope, Dhoom){
    $scope.data = Dhoom;
}
